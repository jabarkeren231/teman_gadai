package com.gaday.temangadai;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Button bt_login,bt_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        bt_register = (Button) findViewById(R.id.bt_register);
        bt_login = (Button) findViewById(R.id.bt_login);

        bt_login.setOnClickListener(this);
        bt_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
       if (v == bt_login){
           Intent i = new Intent(LoginActivity.this,MainActivity.class);
           startActivity(i);
           finish();
       }if (v == bt_register){
           register();
       }
    }

    private void register() {
        Intent i = new Intent(LoginActivity.this,RegistrationActivity.class);
        startActivity(i);
        finish();
    }
}
