package com.gaday.temangadai;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class FragmentSingle extends Fragment implements SingleListAdapter.OnGridItemSelectedListener{

    private RecyclerView lvSingle;
    private GridLayoutManager gridLayoutManager;
    private SingleListAdapter singleListAdapter;
    private Context context;

    public static FragmentSingle newInstance() {
        return new FragmentSingle();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_single, container, false);
        lvSingle = (RecyclerView) rootView.findViewById(R.id.lvSingle);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        singleListAdapter = new SingleListAdapter(this);
        // grid 2 kolom
        gridLayoutManager = new GridLayoutManager(context, 2);
        lvSingle.setLayoutManager(gridLayoutManager);

        // set margin 2 dp
        lvSingle.addItemDecoration(new GridMarginDecoration(context, 2, 2, 2, 2));
        lvSingle.setAdapter(singleListAdapter);
        loadData();
    }

    private void loadData(){
        List<Single> singleList = new ArrayList<>();
        Single single;

        //Ambil Gambar Dari API
        int img[] = {R.mipmap.ic_launcher,R.mipmap.ic_launcher,
                R.mipmap.ic_launcher, R.mipmap.ic_launcher,
                R.mipmap.ic_launcher, R.mipmap.ic_launcher};

        //Ambil Judul Dari API
        String title[] = {"AKB48 43rd Single - Kimi wa Melody", "AKB48 42nd Single - Kuchibiru ni Be My Baby",
                "AKB48 41st Single - Halloween Night", "AKB48 40th Single - Bokutachi wa Tatakawanai",
                "AKB48 39th Single - Green Flash", "AKB48 38th Single - Kibouteki Refrain"};

        String price[] = {"Rp50.000", "Rp50.000",
                "Rp50.000", "Rp50.000",
                "Rp50.000", "Rp50.000"};

        for (int i = 0; i < img.length; i++){
            single = new Single();
            single.setImg(img[i]);
            single.setTitle(title[i]);
            single.setPrice(price[i]);
            singleList.add(single);
        }
        singleListAdapter.addAll(singleList);
    }

    @Override
    public void onGridItemClick(View v, int position) {
        produk();
    }

    private void produk() {
        Intent intent = new Intent(context, DetailProdukActivity.class);
        context.startActivity(intent);
    }

}

