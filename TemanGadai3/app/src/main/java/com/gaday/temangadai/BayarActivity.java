package com.gaday.temangadai;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class BayarActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_selesai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        btn_selesai = (Button) findViewById(R.id.btn_selesai);

        btn_selesai.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(BayarActivity.this,VerivikasiActivity.class);
        startActivity(i);
    }
}
