package com.gaday.temangadai;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Button bt_login,bt_register;
    EditText et_email2,et_password2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        bt_register = (Button) findViewById(R.id.bt_register);
        bt_login = (Button) findViewById(R.id.bt_login);
        et_email2 = (EditText)findViewById(R.id.et_email);
        et_password2 = (EditText)findViewById(R.id.et_password);
        bt_login.setOnClickListener(this);
        bt_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
       if (v == bt_login){
           String email = et_email2.getText().toString();
           String password = et_password2.getText().toString();

           if(email == "faisal@gmail.com" && password == "faisalkontol"){

               Toast.makeText(this, "You Signed In", Toast.LENGTH_SHORT).show();
               Intent i = new Intent(LoginActivity.this,MainActivity.class);
               startActivity(i);
               finish();
           }else{
               Toast.makeText(this, "Invalid Login", Toast.LENGTH_SHORT).show();
           }

       }if (v == bt_register){
           register();
       }
    }

    private void register() {
        Intent i = new Intent(LoginActivity.this,RegistrationActivity.class);
        startActivity(i);
        finish();
    }
}
