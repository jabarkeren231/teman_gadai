package com.gaday.temangadai;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DpActivity extends AppCompatActivity implements View.OnClickListener {

    Button bt_lanjutkan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dp);

        bt_lanjutkan = (Button) findViewById(R.id.bt_lanjutkan);

        bt_lanjutkan.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == bt_lanjutkan){
            Intent i = new Intent(DpActivity.this,BayarActivity.class);
            startActivity(i);
        }
    }
}
